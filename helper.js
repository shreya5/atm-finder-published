const AWS = require('aws-sdk');
const _ = require('lodash');

const docClient = new AWS.DynamoDB.DocumentClient();

const googleApiClient = require('@google/maps').createClient({
  key: 'AIzaSyA8SPUIMlxaMpC2s0yzAMt7DOrNqt844mY'
});

const algoliasearch = require('algoliasearch');
const algoliaClient = algoliasearch('XR8JO0ZLBQ','b5b86ca2888abd3a1a2d36f22b6ae6a7');
const algoliaIndex = algoliaClient.initIndex('locations');

const SlackWebhook = require('slack-webhook');
const slack = new SlackWebhook('https://hooks.slack.com/services/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');

exports.getData = () => {

  const params = {
    TableName : 'location-list'
  };

  return new Promise((resolve, reject) => {

    docClient.scan(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);

      }

    });

  });

};

exports.findGeoCode = addressText => {

  return new Promise((resolve, reject) => {

    googleApiClient.geocode({
      address: addressText
    }, (err, response) => {
      if (err) {
        reject(err);
      }

      if(response.json.results.length > 0) {
        const geometry = response.json.results[0].geometry;
        resolve(geometry.location);
      } else {
        resolve(null);
      }
    });

  });

};

exports.startStateMachine = location => {

  const params = {
    stateMachineArn: 'arn:aws:states:us-east-2:775695676505:stateMachine:atm-finder-v2',
    input: JSON.stringify(location)
  };

  const stepfunctions = new AWS.StepFunctions();
  stepfunctions.startExecution(params, (err, data) => {

    if (err) {
      console.err(err);
    }
    else {
      console.log('State Machine started successfully');
      console.log(data);
    }

  });
};

exports.pushToAlgolia = location => {
  return algoliaIndex.addObject(location);
};

exports.sendToSlack = message => {
 slack.send(message)
   .then(data => {
    console.log(data);
   })
   .catch(err => {
    console.log(err);
   })
};


exports.removeFromAlgolia = locationId => {
  return algoliaIndex.deleteObject(locationId);
};

exports.searchAlgolia = geocodes => {

  return algoliaIndex.search({
    aroundLatLng: `${geocodes.lat}, ${geocodes.lng}`,
    aroundRadius: 7000
  });
};